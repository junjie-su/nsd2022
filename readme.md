- 笔记下载方式：

>  注意：
>
> windows系统，先安装git软件。git软件在教学环境软件的【云计算学院】-->【基础环境-所有阶段都需要】目录中。
>

课上笔记如果本地没有任何资料，第一次下载：

```shell
# Linux系统
[root@localhost ~]# yum -y install git
[root@localhost ~]# git clone https://gitee.com/mrzhangzhg/nsd2022.git
# 上述命令执行完成后，将会出现nsd2022笔记目录

# Windows系统，先按 windows键+r ，输入cmd回车，进入命令行
C:\User\Admin> d:    # 命令行中直接输入d:回车，即可进入d盘
D:\> git clone https://gitee.com/mrzhangzhg/nsd2022.git
# 上述命令执行完成后，D盘将会出现nsd2022笔记目录
```
后续同步：
```
# cd nsd2022
# git pull
```

- 本笔记采用互联网最为流行的Markdown文本格式，通过Typora编写。
  - windows版本的Typora软件在教学环境软件的【云计算学院】-->【基础环境-所有阶段都需要】目录中。
  
  - Linux系统下，先运行笔记目录中【素材】目录中的`config_md.sh`脚本，然后就可以双击打开扩展名为md的笔记文件了。
- 云计算学院2022年课程全阶段软件：
  链接：https://pan.baidu.com/s/1H9BCY_dgnmgVKgpQceDKDQ 
  提取码：Va2M
- 云计算脱产学员（VMware版本虚拟机实验环境）:
  链接：https://pan.baidu.com/s/1qvu6hjbagDhngh4-t4stvA?pwd=1234

- 我的Python专辑：《Python百例》
  - https://www.jianshu.com/c/00c61372c46a
  - https://cloud.tencent.com/developer/column/5283

市面上“从入门到精通”的书很多，但是很时候反而成了“从入门到放弃”。我并不打算以“从入门到精通”的名号，把大家搞到放弃。所以写了一本“从入门到菜鸟”的书。

本书注重基础和编程思路的讲解，通过一系列示例帮助读者顺利入门。

书名为《例解Python》，已由电子工业出版社出版，在京东等各大电商平台有售。[京东链接](https://item.jd.com/13054450.html)
![](imgs/py.png)