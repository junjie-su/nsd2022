#!/bin/bash

cat <<EOF > /tmp/x-markdown.xml
<?xml version="1.0"?>
<mime-info xmlns='http://www.freedesktop.org/standards/shared-mime-info'
>
  <mime-type type="text/x-markdown">
    <comment>markdown file type</comment>
    <glob pattern="*.md"/>
  </mime-type>
</mime-info>
EOF

echo '正在配置，请耐心等待一小会'
xdg-mime install /tmp/x-markdown.xml
xdg-mime default typora.desktop x-markdown
echo -e '\033[32;1m配置完成\033[0m'
