#!/bin/bash

mysql -e "create database wordpress character set utf8mb4"
mysql -e 'grant all on wordpress.* to wordpress@"localhost" identified by "wordpress"'
mysql -e 'grant all on wordpress.* to wordpress@"192.168.2.11" identified by "wordpress"'
mysql -e 'grant all on wordpress.* to wordpress@"%" identified by "wordpress"'
