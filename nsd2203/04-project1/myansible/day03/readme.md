# 文件说明
[toc]
- `ansible.cfg`：主配置文件
- `inventory`：主机清单文件
- `files`目录：用于保存需要上传的文件
- `roles`目录：角色目录
- `roles/inst_pkg`目录：用于装包的角色
- `roles/enable_svr`目录：用于起服务的角色
- `scripts`目录：用于保存在服务器上运行的脚本
- `01_upload_repo_proxy.yml`：配置yum
- `02_inst_haproxy.yml`：安装haproxy
- `03_config_haproxy.yml`：配置并启动haproxy
- `04_inst_keepalived.yml`：安装keepalived服务
- `05_config_keepalived.yml`：配置并起动keepalived
- `06_stop_nginx.yml`：停止nginx，卸载html目录
- `07_inst_cephclient.yml`：在webservers上安装ceph客户端工具
- `08_mount_ceph.yml`：挂载ceph fs到webservers
- `09_get_nfs_html.yml`：将nfs的共享html目录打包并下载
- `10_unarchive_html.yml`：将下载的html压缩包解压到web1
- `11_start_nginx.yml`：起动nginx服务
